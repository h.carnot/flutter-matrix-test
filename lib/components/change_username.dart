import 'package:flutter/material.dart';
import 'package:matrix_famedly_test/matrix_widget.dart';

class ChangeUsername extends StatefulWidget {
  const ChangeUsername({Key? key}) : super(key: key);

  @override
  State<ChangeUsername> createState() => _ChangeUsernameState();
}

class _ChangeUsernameState extends State<ChangeUsername> {
  TextEditingController controller = TextEditingController();

  Future<void> _loadUserName() async {
    final client = Matrix.of(context).sclient!;

    String? username = client.userID != null
        ? (await client.getProfileFromUserId(client.userID!)).displayName
        : null;

    if (username != null && controller.text == "") controller.text = username;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _loadUserName(),
        builder: (context, snapshot) {
          return Card(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      const Text("Username : "),
                      Flexible(child: TextField(controller: controller))
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: OutlinedButton(
                        child: const Text("Change name"),
                        onPressed: () {
                          if (Matrix.of(context).sclient!.userID != null) {
                            Matrix.of(context).sclient!.setDisplayName(
                                Matrix.of(context).sclient!.userID!,
                                controller.text);
                          }
                        }),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
