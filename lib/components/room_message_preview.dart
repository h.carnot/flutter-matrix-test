import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:matrix_famedly_test/page/states_page.dart';

import '../matrix_widget.dart';
import '../page/room_encryption_settings_page.dart';
import '../page/room_history.dart';

class RoomMessagePreview extends StatelessWidget {
  const RoomMessagePreview({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Matrix.of(context).sclient!.roomsLoading,
        builder: (context, snapshot) {
          return Column(
            children: [
              for (Room r in Matrix.of(context).sclient!.rooms
                ..sort((a, b) => a.lastEvent != null && b.lastEvent != null
                    ? b.lastEvent!.originServerTs
                        .compareTo(a.lastEvent!.originServerTs)
                    : 1))
                RoomButton(room: r)
            ],
          );
        });
  }
}

class RoomButton extends StatefulWidget {
  const RoomButton({Key? key, required this.room}) : super(key: key);

  final Room room;

  @override
  State<RoomButton> createState() => _RoomButtonState();
}

class _RoomButtonState extends State<RoomButton> {
  late Future<bool> history;
  Future<bool> waitForPostLoad() async {
    await widget.room.postLoad();
    return true;
  }

  @override
  void initState() {
    history = waitForPostLoad();
    super.initState();
  }

  void loadInPoint(BuildContext context, Room r, String eventId) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
                RoomHistory(room: r, eventID: eventId)));
  }

  void states(BuildContext context, Room r) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => StatesPage(r: r)));
  }

  void verifySettings(BuildContext context, Room r) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => RoomEncryptionSettingsPage(room: r)));
  }

  List<String>? getPinnedMessages(Room room) {
    return room
        .getState('m.room.pinned_events')
        ?.content['pinned']
        ?.cast<String>();
  }

  @override
  Widget build(BuildContext context) {
    final r = widget.room;

    final pinnedEvents = getPinnedMessages(r) ?? [];
    return FutureBuilder<bool>(
        future: history,
        builder: (context, snapshot) {
          return r.lastEvent == null
              ? const Text("Null event")
              : Column(
                  children: [
                    ListTile(
                      title: Text(r.displayname + " - " + r.id),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(r.lastEvent!.sender.displayName ?? 'unknown'),
                          Text(r.lastEvent!.senderId),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                RoomSubButton(
                                    text: "States",
                                    onPressed: () => states(context, r)),
                                RoomSubButton(
                                    text: "Verify devices",
                                    onPressed: () =>
                                        verifySettings(context, r)),
                              ],
                            ),
                          ),
                          for (final event in pinnedEvents)
                            FutureBuilder<Event?>(
                                future: r.getEventById(event),
                                builder: (context, snap) {
                                  return Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: RoomSubButton(
                                        text: snap.hasData
                                            ? snap.data!.body
                                            : event,
                                        onPressed: () =>
                                            loadInPoint(context, r, event)),
                                  );
                                })
                        ],
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    RoomHistory(room: r)));
                      },
                    )
                  ],
                );
        });
  }
}

class RoomSubButton extends StatelessWidget {
  const RoomSubButton({Key? key, required this.text, required this.onPressed})
      : super(key: key);

  final String text;
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 2.0),
      child: MaterialButton(
          child: Text(text, style: const TextStyle(color: Colors.white)),
          onPressed: onPressed,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          color: Colors.blue),
    );
  }
}
