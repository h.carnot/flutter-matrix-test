import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:matrix_famedly_test/matrix_widget.dart';
import 'package:matrix_famedly_test/page/welcome_page.dart';
import 'package:minestrix_chat/partials/login/login_card.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Matrix(
      child: MaterialApp(
        title: 'Matrix famedly test',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<bool> initMatrix(Client m) async {
    Logs().i("[ logged ] : " + m.isLogged().toString());

    if (m.isLogged() == false) {
      await m.init(
          waitForFirstSync: false, waitUntilLoadCompletedLoaded: false);
    }
    await m.roomsLoading;

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
          future: initMatrix(Matrix.of(context).sclient!),
          builder: (context, _) {
            return StreamBuilder<LoginState>(
                stream: Matrix.of(context).sclient?.onLoginStateChanged.stream,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const Center(child: CircularProgressIndicator());
                  }

                  return snapshot.data == LoginState.loggedIn
                      ? const WelcomePage()
                      : LoginMatrixPage(client: Matrix.of(context).sclient!);
                });
          }),
    );
  }
}
