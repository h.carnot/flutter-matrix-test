import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:minestrix_chat/view/matrix_chat_page.dart';

import '../matrix_widget.dart';

class ArchivedRoomsPage extends StatefulWidget {
  const ArchivedRoomsPage({Key? key}) : super(key: key);

  @override
  State<ArchivedRoomsPage> createState() => _ArchivedRoomsPageState();
}

class _ArchivedRoomsPageState extends State<ArchivedRoomsPage> {
  Future<List<Room>>? futureRooms;

  @override
  Widget build(BuildContext context) {
    if (futureRooms == null) {
      final client = Matrix.of(context).sclient!;
      futureRooms = client.loadArchive();
    }

    return Scaffold(
      appBar: AppBar(title: const Text("Archived rooms")),
      body: SafeArea(
        child: ListView(
          children: [
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Text("Archived rooms",
                  style: TextStyle(fontWeight: FontWeight.bold)),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FutureBuilder<List<Room>>(
                  future: futureRooms,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const Center(child: CircularProgressIndicator());
                    }

                    final archives = snapshot.data!;
                    if (archives.isEmpty) return const Text("No rooms");
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        for (final archive in archives)
                          ListTile(
                              title: Text(archive.name),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            Scaffold(
                                              body: MatrixChatPage(
                                                client:
                                                    Matrix.of(context).sclient!,
                                                roomId: archive.id,
                                                onBack: () {
                                                  Navigator.of(context).pop();
                                                },
                                              ),
                                            )));
                              },
                              subtitle: Text(archive.topic)),
                      ],
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
