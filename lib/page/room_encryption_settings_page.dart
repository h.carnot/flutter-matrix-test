import 'package:flutter/material.dart';
import 'package:matrix/encryption.dart';
import 'package:matrix/matrix.dart';
import 'package:matrix_famedly_test/utils/device_extension.dart';
import 'package:minestrix_chat/partials/dialogs/key_verification_dialogs.dart';

class RoomEncryptionSettingsPage extends StatefulWidget {
  final Room room;
  const RoomEncryptionSettingsPage({Key? key, required this.room})
      : super(key: key);

  @override
  State<RoomEncryptionSettingsPage> createState() =>
      _RoomEncryptionSettingsPageState();
}

class _RoomEncryptionSettingsPageState
    extends State<RoomEncryptionSettingsPage> {
  Future<void> onSelected(
      BuildContext context, String action, DeviceKeys key) async {
    switch (action) {
      case 'verify':
        final req = key.startVerification();
        req.onUpdate = () {
          if (req.state == KeyVerificationState.done) {
            setState(() {});
          }
        };
        await KeyVerificationDialog(request: req).show(context);
        break;
      case 'verify_user':
        final req = await widget.room.client.userDeviceKeys[key.userId]!
            .startVerification();
        req.onUpdate = () {
          if (req.state == KeyVerificationState.done) {
            setState(() {});
          }
        };
        await KeyVerificationDialog(request: req).show(context);
        break;
      case 'block':
        if (key.directVerified) {
          await key.setVerified(false);
        }
        await key.setBlocked(true);
        setState(() {});
        break;
      case 'unblock':
        setState(() {});
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Verify"),
          elevation: 0,
        ),
        body: ListView(children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ListTile(
                title: const Text("Verify device"),
                leading: CircleAvatar(
                  backgroundColor: Theme.of(context).secondaryHeaderColor,
                  foregroundColor: Theme.of(context).colorScheme.secondary,
                  child: const Icon(Icons.lock),
                ),
              ),
              const Divider(height: 1),
              StreamBuilder(
                  stream: widget.room.onUpdate.stream,
                  builder: (context, snapshot) {
                    return FutureBuilder<List<DeviceKeys>>(
                      future: widget.room.getUserDeviceKeys(),
                      builder: (BuildContext context, snapshot) {
                        if (snapshot.hasError) {
                          return Center(
                            child: Text("oopsSomethingWentWrong : " +
                                snapshot.error.toString()),
                          );
                        }
                        if (!snapshot.hasData) {
                          return const Center(
                              child: CircularProgressIndicator.adaptive(
                                  strokeWidth: 2));
                        }
                        final deviceKeys = snapshot.data!;
                        return ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: deviceKeys.length,
                          itemBuilder: (BuildContext context, int i) => Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              if (i == 0 ||
                                  deviceKeys[i].userId !=
                                      deviceKeys[i - 1].userId) ...{
                                const Divider(height: 1, thickness: 1),
                                PopupMenuButton(
                                  onSelected: (dynamic action) => onSelected(
                                      context, action, deviceKeys[i]),
                                  itemBuilder: (c) {
                                    final items = <PopupMenuEntry<String>>[];
                                    if (widget
                                            .room
                                            .client
                                            .userDeviceKeys[
                                                deviceKeys[i].userId]!
                                            .verified ==
                                        UserVerifiedStatus.unknown) {
                                      items.add(const PopupMenuItem(
                                        value: 'verify_user',
                                        child: Text("verifyUser"),
                                      ));
                                    }
                                    return items;
                                  },
                                  child: ListTile(
                                    title: Text(
                                      widget.room
                                          .getUserByMXIDSync(
                                              deviceKeys[i].userId)
                                          .calcDisplayname(),
                                    ),
                                    subtitle: Text(
                                      deviceKeys[i].userId,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                ),
                              },
                              PopupMenuButton(
                                onSelected: (dynamic action) =>
                                    onSelected(context, action, deviceKeys[i]),
                                itemBuilder: (c) {
                                  final items = <PopupMenuEntry<String>>[];
                                  if (deviceKeys[i].blocked ||
                                      !deviceKeys[i].verified) {
                                    items.add(PopupMenuItem(
                                      value: deviceKeys[i].userId ==
                                              widget.room.client.userID
                                          ? 'verify'
                                          : 'verify_user',
                                      child: const Text("verifyStart"),
                                    ));
                                  }
                                  if (deviceKeys[i].blocked) {
                                    items.add(const PopupMenuItem(
                                      value: 'unblock',
                                      child: Text("unblockDevice"),
                                    ));
                                  }
                                  if (!deviceKeys[i].blocked) {
                                    items.add(const PopupMenuItem(
                                      value: 'block',
                                      child: Text("blockDevice"),
                                    ));
                                  }
                                  return items;
                                },
                                child: ListTile(
                                  leading: CircleAvatar(
                                    foregroundColor: Colors.white,
                                    backgroundColor: deviceKeys[i].color,
                                    child: Icon(deviceKeys[i].icon),
                                  ),
                                  title: Text(
                                    deviceKeys[i].displayname,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  subtitle: Row(
                                    children: [
                                      Text(
                                        deviceKeys[i].deviceId!,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w300),
                                      ),
                                      const Spacer(),
                                      Text(
                                        deviceKeys[i].blocked
                                            ? "blocked"
                                            : deviceKeys[i].verified
                                                ? "verified"
                                                : "unverified",
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: deviceKeys[i].color,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  }),
            ],
          ),
        ]));
  }
}

extension on DeviceKeys {
  Color get color => blocked
      ? Colors.red
      : verified
          ? Colors.green
          : Colors.orange;
}
