import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';

class RoomHistory extends StatefulWidget {
  const RoomHistory({Key? key, required this.room, this.eventID})
      : super(key: key);
  final Room room;
  final String? eventID;
  @override
  State<RoomHistory> createState() => _RoomHistoryState();
}

class _RoomHistoryState extends State<RoomHistory> {
  Future<Timeline?>? futureTimeline;

  bool enableScrolListener = false;
  ScrollController controller = ScrollController();

  bool checkTimelineIntegrity = false; // need a specific timeline
  /*
  To use this timeline integrity verification you need to have a timeline with messages incrementaly ordered
  1
  2
  3
  4
  5
  [...]
   */

  @override
  void initState() {
    super.initState();
    if (widget.eventID != null) {
      futureTimeline = widget.room.getTimelineOnEventContext(
          onUpdate: () {
            if (mounted) setState(() {});
          },
          onNewEvent: onNewEvent,
          centerOnEvent: widget.eventID!);
    } else {
      futureTimeline = widget.room.getTimeline(
          onNewEvent: onNewEvent,
          onUpdate: () {
            if (mounted) setState(() {});
          });
    }

    controller.addListener(listener);
  }

  void onNewEvent() {
    if (controller.hasClients && controller.position.pixels != 0 ||
        !timeline!.allowNewEvent) {
      final snackBar = SnackBar(
        content: const Text('A new message'),
        action: timeline!.allowNewEvent
            ? SnackBarAction(
                label: 'See',
                onPressed: () {
                  controller.animateTo(0,
                      duration: const Duration(milliseconds: 100),
                      curve: Curves.ease);
                },
              )
            : null,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  void listener() async {
    if (!enableScrolListener) return;

    if (controller.position.pixels == 0) {
      await requestFuture();
    } else {
      if (controller.position.pixels + controller.position.viewportDimension >
          controller.position.maxScrollExtent) {
        requestHistory();
      }
    }
  }

  Timeline? timeline;

  bool get canRequest => timeline?.canRequestHistory ?? false;

  Future<void> requestFuture() async {
    await timeline?.requestFuture();
  }

  Future<void> requestHistory() async {
    if (canRequest) {
      await timeline?.requestHistory();
    }
  }

  void getEvent() async {
    if (widget.eventID != null) {
      final e = await widget.room.getEventById(widget.eventID!);
      Logs()
          .w("Get event: ${widget.eventID} - ${e?.originServerTs.toString()}");
    }
  }

  bool checkError(Event next, Event prev) {
    int n = int.tryParse(next.body) ?? 0;
    int p = int.tryParse(prev.body) ?? -10;

    if (next.type != EventTypes.Message ||
        prev.type != EventTypes.Message ||
        next.redacted ||
        prev.redacted) return false;

    if (n != p - 1) {
      Logs().e("Error: $n -> $p ${prev.body} -> ${next.body}");
      return true;
    }
    return false;
  }

  bool checkErrorList(List<Event> events) {
    bool error = false;
    for (var i = 1; i < events.length; i++) {
      if (checkError(events[i], events[i - 1])) {
        error = true;
      }
    }
    return error;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.room.name),
        actions: [
          Row(
            children: [
              const Text("Timeline check"),
              Switch(
                  value: checkTimelineIntegrity,
                  onChanged: (value) => setState(() {
                        checkTimelineIntegrity = value;
                      })),
            ],
          ),
          Row(
            children: [
              const Text("Scroll listener"),
              Switch(
                  value: enableScrolListener,
                  onChanged: (value) => setState(() {
                        enableScrolListener = value;
                      })),
            ],
          )
        ],
      ),
      body: SafeArea(
          child: FutureBuilder<Timeline?>(
              future: futureTimeline,
              builder: (context, snapTimeline) {
                if (snapTimeline.hasError) {
                  Logs().e("Error", snapTimeline.error);
                  return Center(
                    child: SizedBox(
                      width: 300,
                      child: ListTile(
                          title: const Text("Error",
                              style:
                                  TextStyle(fontSize: 16, color: Colors.red)),
                          subtitle: Text((snapTimeline.error).toString())),
                    ),
                  );
                }

                if (snapTimeline.data == null) {
                  return Center(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: const [
                        CircularProgressIndicator(),
                        SizedBox(width: 20),
                        Text("Loading timeline")
                      ],
                    ),
                  );
                }

                timeline = snapTimeline.data;

                bool errorInTimeline = checkTimelineIntegrity
                    ? checkErrorList(timeline!.events)
                    : false;

                return Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          if (canRequest)
                            TextButton(
                                child: const Text("Request history"),
                                onPressed: () async {
                                  await requestHistory();
                                }),
                          Expanded(
                            child: ListView.builder(
                                controller: controller,
                                reverse: true,
                                itemCount: timeline?.events.length,
                                itemBuilder: (context, pos) =>
                                    Builder(builder: (context) {
                                      bool error = false;
                                      Event e = timeline!.events[pos];

                                      if (checkTimelineIntegrity) {
                                        final e = timeline!.events[pos];
                                        if (pos > 0) {
                                          final prev =
                                              timeline!.events[pos - 1];
                                          if (checkError(e, prev)) {
                                            error = true;
                                          }
                                        }
                                      }

                                      return MaterialButton(
                                        onPressed: () {},
                                        child: Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Flexible(
                                                child: Card(
                                                    color: error
                                                        ? Colors.red
                                                        : Colors.blue,
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: Text(e.body,
                                                          style:
                                                              const TextStyle(
                                                                  color: Colors
                                                                      .white)),
                                                    )),
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Text(
                                                      e.originServerTs
                                                          .toString(),
                                                      style: const TextStyle(
                                                          color: Colors.grey,
                                                          fontWeight:
                                                              FontWeight.bold)),
                                                  Text(e.eventId,
                                                      style: const TextStyle(
                                                          color: Colors.grey)),
                                                  if (e.status !=
                                                      EventStatus.synced)
                                                    Text(e.status.toString())
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    })),
                          ),
                          if (timeline != null)
                            TextButton(
                                child: const Text("Request future"),
                                onPressed: () async {
                                  await requestFuture();
                                }),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        if (checkTimelineIntegrity)
                          SizedBox(
                            width: 300,
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Card(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                        padding: const EdgeInsets.all(12.0),
                                        child: Card(
                                            color: checkTimelineIntegrity
                                                ? (errorInTimeline
                                                    ? Colors.red
                                                    : Colors.green)
                                                : Colors.blue,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      Text(
                                                          checkTimelineIntegrity
                                                              ? "Timeline status"
                                                              : "Timeline",
                                                          style:
                                                              const TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize:
                                                                      18)),
                                                    ],
                                                  ),
                                                  if (checkTimelineIntegrity)
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 10.0),
                                                      child: Row(
                                                        children: [
                                                          Icon(
                                                              errorInTimeline
                                                                  ? Icons.error
                                                                  : Icons
                                                                      .check_circle,
                                                              color:
                                                                  Colors.white),
                                                          const SizedBox(
                                                              width: 12),
                                                          Text(
                                                              errorInTimeline
                                                                  ? "Error"
                                                                  : "Ok",
                                                              style: const TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize:
                                                                      15)),
                                                        ],
                                                      ),
                                                    ),
                                                ],
                                              ),
                                            ))),
                                    Column(
                                      children: [
                                        ListTile(
                                            title: const Text("Prev batch"),
                                            subtitle: Text(
                                                timeline!.chunk.prevBatch)),
                                        ListTile(
                                            title: const Text("Next batch"),
                                            subtitle: Text(
                                                timeline!.chunk.nextBatch)),
                                        ListTile(
                                            title: const Text("Length"),
                                            subtitle: Text(
                                                '${timeline!.chunk.events.length} items')),
                                        ListTile(
                                          title: const Text(
                                              "Timeline fragment status"),
                                          subtitle: Card(
                                              color:
                                                  timeline!.isFragmentedTimeline
                                                      ? Colors.orange
                                                      : Colors.green,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(4.0),
                                                child: Text(
                                                    timeline!
                                                            .isFragmentedTimeline
                                                        ? "Fragmented"
                                                        : "Sync",
                                                    style: const TextStyle(
                                                        color: Colors.white)),
                                              )),
                                        ),
                                        ListTile(
                                          title: const Text("Allow new events"),
                                          subtitle: Card(
                                              color: timeline!.allowNewEvent
                                                  ? Colors.green
                                                  : Colors.orange,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(4.0),
                                                child: Text(
                                                    timeline!.allowNewEvent
                                                        .toString(),
                                                    style: const TextStyle(
                                                        color: Colors.white)),
                                              )),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        /*
                          FutureBuilder<TimelineFragmentList?>(
                              future: () async {}(),
                              builder: (context, snapFrag) {
                                return SizedBox(
                                    width: 360,
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Card(
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Padding(
                                                  padding: const EdgeInsets.all(
                                                      12.0),
                                                  child: Card(
                                                      color: Colors.blue,
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(8.0),
                                                        child: Row(
                                                          children: const [
                                                            Text(
                                                                "Fragments explorer",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize:
                                                                        18)),
                                                          ],
                                                        ),
                                                      ))),
                                              Builder(builder: (context) {
                                                if (snapFrag.hasError) {
                                                  return ListTile(
                                                      title:
                                                          const Text("Error"),
                                                      subtitle: Text(snapFrag
                                                          .error
                                                          .toString()));
                                                }

                                                if (!snapFrag.hasData) {
                                                  return const Center(
                                                      child:
                                                          CircularProgressIndicator());
                                                }

                                                final keys =
                                                    snapFrag.data!.fragmentKeys;

                                                return Expanded(
                                                  child: ListView.builder(
                                                      itemCount: keys.length,
                                                      itemBuilder:
                                                          (context, i) =>
                                                              Builder(builder:
                                                                  (context) {
                                                                final frag = snapFrag
                                                                    .data!
                                                                    .getFragment(
                                                                        keys[i],
                                                                        followRedirect:
                                                                            false);
                                                                return ListTile(
                                                                    title: Row(
                                                                      children: [
                                                                        frag?.isRoot ??
                                                                                false
                                                                            ? const Card(
                                                                                color: Colors.green,
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.all(8.0),
                                                                                  child: Text("root", style: TextStyle(color: Colors.white)),
                                                                                ))
                                                                            : Text(keys[i]),
                                                                        if (frag?.isRedirect ??
                                                                            false)
                                                                          const Card(
                                                                              color: Colors.orange,
                                                                              child: Padding(
                                                                                padding: EdgeInsets.all(8.0),
                                                                                child: Text("replaced", style: TextStyle(color: Colors.white)),
                                                                              ))
                                                                      ],
                                                                    ),
                                                                    subtitle:
                                                                        Column(
                                                                      children: [
                                                                        if (frag !=
                                                                            null)
                                                                          Column(
                                                                            children: [
                                                                              if (frag.prevBatch != "")
                                                                                ListTile(title: const Text("Prev batch"), subtitle: Text(frag.prevBatch)),
                                                                              if (frag.nextBatch != "")
                                                                                ListTile(title: const Text("Next batch"), subtitle: Text(frag.nextBatch)),
                                                                              if (frag.fragmentId != keys[i])
                                                                                ListTile(
                                                                                    title: const Text("Fragment id"),
                                                                                    subtitle: Card(
                                                                                        color: Colors.red,
                                                                                        child: Padding(
                                                                                          padding: const EdgeInsets.all(4),
                                                                                          child: Text('${frag.fragmentId} | ${keys[i]}', style: const TextStyle(color: Colors.white)),
                                                                                        ))),
                                                                              ListTile(title: const Text("Length"), subtitle: Text('${frag.eventsId.length} items')),
                                                                              if (frag.isRedirect)
                                                                                ListTile(title: const Text("Redirect"), subtitle: Text('${frag.replacedBy}')),
                                                                            ],
                                                                          ),
                                                                      ],
                                                                    ));
                                                              })),
                                                );
                                              }),
                                            ]),
                                      ),
                                    ));
                              })
                    */
                      ],
                    )
                  ],
                );
              })),
    );
  }
}
