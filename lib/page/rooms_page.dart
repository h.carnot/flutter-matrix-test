import 'package:flutter/material.dart';

import '../components/room_message_preview.dart';

class RoomsPage extends StatefulWidget {
  const RoomsPage({Key? key}) : super(key: key);

  @override
  State<RoomsPage> createState() => _RoomsPageState();
}

class _RoomsPageState extends State<RoomsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Rooms")),
        body: ListView(
          children: const [
            RoomMessagePreview(),
          ],
        ));
  }
}
