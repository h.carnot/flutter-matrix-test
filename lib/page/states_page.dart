import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:json_editor/json_editor.dart';
import 'package:matrix/matrix.dart';

class StatesPage extends StatefulWidget {
  final Room r;
  const StatesPage({Key? key, required this.r}) : super(key: key);

  @override
  State<StatesPage> createState() => _StatesPageState();
}

class _StatesPageState extends State<StatesPage> {
  TextEditingController c = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: widget.r.postLoad(),
        builder: (context, snapshot) {
          return Scaffold(
              appBar: AppBar(title: Text("states - " + widget.r.displayname)),
              body: SafeArea(
                  child: ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                        controller: c, onChanged: (_) => setState(() {})),
                  ),
                  for (var state in widget.r.states.entries
                      .where((element) => element.key.contains(c.text)))
                    ListTile(
                        title: Text(state.key),
                        leading: const Icon(Icons.insert_emoticon),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            for (var stateItem in state.value.entries)
                              Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Card(
                                  child: Padding(
                                    padding: const EdgeInsets.all(4),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text("key: " + stateItem.key,
                                            style: const TextStyle(
                                                fontWeight: FontWeight.bold)),
                                        ConstrainedBox(
                                          constraints: const BoxConstraints(
                                              maxHeight: 160),
                                          child: JsonEditor.string(
                                            enabled: false,
                                            jsonString: jsonEncode(
                                                stateItem.value.content),
                                            onValueChanged: (value) {},
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                          ],
                        )),
                ],
              )));
        });
  }
}
