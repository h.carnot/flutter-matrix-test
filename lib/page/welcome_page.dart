import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:matrix_famedly_test/matrix_widget.dart';
import 'package:matrix_famedly_test/page/archived_rooms_page.dart';
import 'package:matrix_famedly_test/page/rooms_page.dart';
import 'package:minestrix_chat/view/matrix_chats_page.dart';

import 'account_settings_page.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text("Do some tests"),
              ),
              MainNavButton(
                text: "Chats",
                builder: (BuildContext context) =>
                    MatrixChatsPage(client: Matrix.of(context).sclient!),
              ),
              MainNavButton(
                  text: "Rooms",
                  builder: (BuildContext context) => const RoomsPage()),
              MainNavButton(
                  text: "Archived rooms",
                  builder: (BuildContext context) => const ArchivedRoomsPage()),
              MainNavButton(
                  text: "Settings",
                  builder: (BuildContext context) =>
                      const AccountSettingsPage()),
              Padding(
                padding: const EdgeInsets.all(20),
                child: MaterialButton(
                    color: Colors.blue,
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Sync",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    onPressed: () async {
                      final client = Matrix.of(context).sclient!;
                      Logs().w("Sync");
                      await client.handleSync(await client.sync());
                      Logs().w("Sync done");
                    }),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: MaterialButton(
                    color: Colors.orange,
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Clear cache",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    onPressed: () async {
                      final client = Matrix.of(context).sclient!;
                      Logs().w("Clearing cache");
                      await client.database?.clearCache();
                      Logs().w("Clearing done");
                    }),
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: MaterialButton(
                    color: Colors.red,
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        "Log out",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    onPressed: () {
                      Matrix.of(context).sclient!.logout();
                    }),
              ),
              const LogLevel()
            ],
          ),
        ),
      ],
    );
  }
}

class LogLevel extends StatefulWidget {
  const LogLevel({Key? key}) : super(key: key);

  @override
  State<LogLevel> createState() => _LogLevelState();
}

class _LogLevelState extends State<LogLevel> {
  void changeLogLevel(Level? level) {
    if (level != null) {
      setState(() {
        Logs().level = level;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text("Log level"),
        RadioListTile(
            title: const Text("Debug"),
            subtitle: const Text("Hmm... so much noise"),
            value: Level.debug,
            groupValue: Logs().level,
            onChanged: changeLogLevel),
        RadioListTile(
            title: const Text("Info"),
            value: Level.info,
            groupValue: Logs().level,
            onChanged: changeLogLevel),
        RadioListTile(
            title: const Text("Verbose"),
            value: Level.verbose,
            groupValue: Logs().level,
            onChanged: changeLogLevel),
        RadioListTile(
            title: const Text("Warning"),
            value: Level.warning,
            groupValue: Logs().level,
            onChanged: changeLogLevel),
        RadioListTile(
            title: const Text("Error"),
            value: Level.error,
            groupValue: Logs().level,
            onChanged: changeLogLevel),
      ],
    );
  }
}

class MainNavButton extends StatelessWidget {
  const MainNavButton({Key? key, required this.text, required this.builder})
      : super(key: key);

  final String text;
  final Widget Function(BuildContext) builder;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListTile(
          title: Text(text),
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: builder));
          }),
    );
  }
}
